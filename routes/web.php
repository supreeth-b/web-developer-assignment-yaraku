<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//CRUD
Route::get('/', 'App\Http\Controllers\BookController@index')->name('books.index');
Route::post('/books', 'App\Http\Controllers\BookController@store')->name('books.store');
Route::get('/books/create', 'App\Http\Controllers\BookController@create')->name('books.create');
Route::put('/books/{book}', 'App\Http\Controllers\BookController@update')->name('books.update');
Route::delete('/books/{book}', 'App\Http\Controllers\BookController@destroy')->name('books.destroy');

//search
Route::get('/books/search', 'App\Http\Controllers\BookController@find')->name('books.find');

//export - CSV
Route::get('/books/export/csv', 'App\Http\Controllers\BookController@exportCSV')->name('books.export.csv');
Route::get('/books/export/csv/title', 'App\Http\Controllers\BookController@exportTitlesCSV')->name('books.export.title.csv');
Route::get('/books/export/csv/author', 'App\Http\Controllers\BookController@exportAuthorsCSV')->name('books.export.author.csv');

//export - XML
Route::get('/books/export/xml', 'App\Http\Controllers\BookController@exportXML')->name('books.export.xml');
Route::get('/books/export/xml/title', 'App\Http\Controllers\BookController@exportTitlesXML')->name('books.export.title.xml');
Route::get('/books/export/xml/author', 'App\Http\Controllers\BookController@exportAuthorsXML')->name('books.export.author.xml');