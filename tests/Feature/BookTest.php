<?php

namespace Tests\Feature;

use App\Exports\BooksExportCSV;
use App\Models\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Maatwebsite\Excel\Facades\Excel;

class BookTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_list_of_books_is_shown()
    {
        //Given we have book in the database
        $book = Book::factory()->create();

        //When user visits the books page
        $response = $this->get('/');

        //Check the response code
        $response->assertOk();

        //They should be able to read the book
        $response->assertSee($book->title);
    }

    /** @test */
    public function a_book_can_be_added()
    {
        $book = Book::factory()->make();
        $this->post('/books', $book->toArray());
        $this->assertCount(1, Book::all());
    }

    /** @test */
    public function a_book_requires_a_title()
    {
        $book = Book::factory()->make([
            'title' => null
        ]);

        $response = $this->post('/books', $book->toArray());
        $response->assertSessionHasErrors('title');
    }

    /** @test */
    public function a_book_requires_an_author()
    {
        $book = Book::factory()->make([
            'author' => null
        ]);

        $response = $this->post('/books', $book->toArray());
        $response->assertSessionHasErrors('author');
    }

    /** @test */
    public function a_book_requires_a_description()
    {
        $book = Book::factory()->make([
            'description' => null
        ]);

        $response = $this->post('/books', $book->toArray());
        $response->assertSessionHasErrors('description');
    }

    /** @test */
    public function a_book_author_can_be_edited()
    {
        $book = Book::factory()->create();
        $book->author = 'New Author';
        $this->put('/books/'. $book->id, $book->toArray());
        $this->assertDatabaseHas('books', [
            'id' => $book->id,
            'author' => 'New Author',
        ]);
    }

    /** @test */
    public function a_book_can_be_deleted()
    {
        $book = Book::factory()->create();
        $this->delete('/books/' . $book->id);
        $this->assertDatabaseMissing('books', [
            'id' => $book->id
        ]);
    }

    /** @test */
    public function a_book_csv_export_can_be_downloaded()
    {
        Book::factory()->create([
            'title' => 'new book',
            'author' => 'some author',
        ]);
        
        Excel::fake();
        
        $response = $this->get('/books/export/csv');

        $response->assertOk();

        Excel::assertDownloaded('books.csv', function (BooksExportCSV $export) {
            // Assert that the correct export is downloaded.
            return $export->collection()->contains('title', 'new book');
        });
    }

    /** @test */
    public function a_book_xml_export_can_be_downloaded()
    {
        $response = $this->get('/books/export/xml');
        
        $response->assertOk();
        
        // Assert that the correct export is downloaded.
        $response->assertDownload('books.xml');
    }
}