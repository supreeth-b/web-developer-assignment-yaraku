## Requirements
- [PHP 7.4](https://docs.docker.com/install)
- [Laravel 8.x](https://www.php.net)
- [Composer](https://getcomposer.org)

## Hosting
- [Click here to access the application](http://yarakuassignment-env.eba-bgqjjt2n.ap-south-1.elasticbeanstalk.com)
- The laravel application has been hosted on AWS via Elastic Beanstalk.
- Database used: AWS RDS - MySQL

## Application Functions
- Add a book to the list.
- Delete a book from the list.
- Change an authors name.
- Sort by title or author.
- Search for a book by title or author.
- Export the the following in CSV and XML
    - A list with Title and Author
    - A list with only Titles
    - A list with only Authors

## CI/CD
- Setup a bitbucket pipeline to automate build and run tests with every update to the repository code.
- Added deployment step to the pipeline to automatically deploy the newer version to AWS EBS.

## Libraries/Dependencies Used
- UI: [Bootstrap 5](https://getbootstrap.com/)
- Icons: [Bootstrap Icons](https://icons.getbootstrap.com/)
- CSV Export: [Laravel Excel](https://laravel-excel.com)
- Column Sort: [Kyslik](https://github.com/Kyslik/column-sortable)

## Run Tests
1. Clone the repository.
1. Run `composer install` to install composer dependencies.
1. Run `php artisan test` command to execute all the tests.
