<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use App\Exports\BooksExportCSV;
use XMLWriter;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::sortable()->orderBy('id', 'asc')->paginate(7);

        return view('books.index', compact('books'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'title' => 'required',
           'author' => 'required',
           'description' => 'required'
       ]);

        Book::create($request->all());
        return redirect()->route('books.index')->with('success', 'Book added.');
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $request->validate([
            'author' => 'required',
        ]);

        $book->update($request->all());

        return redirect()->route('books.index')->with('success', 'Book author updated.');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect()->route('books.index')->with('success', 'Book deleted.');
    }
    

    /**
     * Search records by title or author
     */
    public function find(Request $request)
    {
        if ($request->ajax()) {
            $output = "";
            $books_matching_title_or_author = Book::where('title', 'LIKE', '%'.$request->search.'%')
            ->orWhere('author', 'LIKE', '%'.$request->search.'%')
            ->orderBy('id', 'asc')
            ->take(7)
            ->get();

            if ($books_matching_title_or_author) {
                foreach ($books_matching_title_or_author as $book) {
                    $action = view('books.action', compact('book'))->render();
                    $output.=
                    "<tr>".
                    "<th>".$book->id."</th>".
                    "<td>".$book->title."</td>".
                    "<td>".$book->author."</td>".
                    "<td class='d-none d-lg-table-cell'>".$book->description."</td>".
                    "<td>".$action."</td>".
                    "</tr>";
                }
            }

            return Response($output);
        }
    }
    

    /**
     * Export titles and authors of all records in CSV format
     * */
    public function exportCSV()
    {
        return (new BooksExportCSV(''))->download('books.csv');
    }

    /**
     * Export only titles of all records in CSV format
     * */
    public function exportTitlesCSV()
    {
        return (new BooksExportCSV('title'))->download('book-titles.csv');
    }

    
    /**
     * Export only authors of all records in CSV format
     * */
    public function exportAuthorsCSV()
    {
        return (new BooksExportCSV('author'))->download('book-authors.csv');
    }

    
    /**
     * Export titles and authors of all records in XML format
     * */
    public function exportXML()
    {
        $books = Book::all();

        $xml = new XMLWriter();
        $xml->openMemory();
        $xml->startDocument();
        $xml->startElement('books');
        
        foreach ($books as $book) {
            $xml->startElement('data');
            $xml->writeAttribute('title', $book->title);
            $xml->writeAttribute('author', $book->author);
            $xml->endElement();
        }
        
        $xml->endElement();
        $xml->endDocument();

        $content = $xml->outputMemory();
        $xml = null;

        $filename = 'books.xml';
        return response()->streamDownload(function () use ($content) {
            echo $content;
        }, $filename);
    }
    

    /**
     * Export titles of all records in XML format
     * */
    public function exportTitlesXML()
    {
        $books = Book::all();

        $xml = new XMLWriter();
        $xml->openMemory();
        $xml->startDocument();
        $xml->startElement('books');
        
        foreach ($books as $book) {
            $xml->startElement('data');
            $xml->writeAttribute('title', $book->title);
            $xml->endElement();
        }
        
        $xml->endElement();
        $xml->endDocument();

        $content = $xml->outputMemory();
        $xml = null;

        $filename = 'book-titles.xml';
        return response()->streamDownload(function () use ($content) {
            echo $content;
        }, $filename);
    }
    

    /**
     * Export authors of all records in XML format
     * */
    public function exportAuthorsXML()
    {
        $books = Book::all();

        $xml = new XMLWriter();
        $xml->openMemory();
        $xml->startDocument();
        $xml->startElement('books');
        
        foreach ($books as $book) {
            $xml->startElement('data');
            $xml->writeAttribute('author', $book->author);
            $xml->endElement();
        }
        
        $xml->endElement();
        $xml->endDocument();

        $content = $xml->outputMemory();
        $xml = null;

        $filename = 'book-authors.xml';
        return response()->streamDownload(function () use ($content) {
            echo $content;
        }, $filename);
    }
}