<?php

namespace App\Exports;

use App\Models\Book;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class BooksExportCSV implements FromCollection, WithMapping, WithHeadings
{
    use Exportable;

    public function __construct(String $column_to_be_exported)
    {
        $this->column = $column_to_be_exported;
    }

    
    public function collection()
    {
        return Book::all();
    }


    /**
     * Map data column headings as per column constraints
     */
    public function headings(): array
    {
        $export_author = $this->column == 'author' ? true : false;
        $export_title =  $this->column == 'title' ? true : false;

        if ($export_author) {
            return [
                '#Author'
            ];
        } elseif ($export_title) {
            return [
                '#Title'
            ];
        } else {
            return [
                '#Title',
                '#Author'
            ];
        }
    }

    
    /**
    * @var Book $book
    * Explicitly mapping the export columns
    */
    public function map($book): array
    {
        $export_author = $this->column == 'author' ? true : false;
        $export_title =  $this->column == 'title' ? true : false;

        if ($export_author) {
            return [
                $book->author
            ];
        } elseif ($export_title) {
            return [
                $book->title
            ];
        } else {
            return [
                $book->title,
                $book->author
            ];
        }
    }
}