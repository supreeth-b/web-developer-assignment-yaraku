@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">

        <!-- Action alert badge -->
        <div class="col-lg-10">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Success!</strong> {{$message}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
        </div>

        <!-- Table -->
        <div class="col-lg-10 rounded shadow">

            <!-- table head -->
            <div class="row bg-light px-2 py-4">
                <div class="d-flex justify-content-between">
                    <!-- Add button -->
                    <div>
                        <a href="{{route('books.create')}}" class="btn px-3 btn-success rounded-pill">
                            <span class="d-flex">Add&nbsp;<i class="bi bi-plus-circle"></i></span>
                        </a>
                    </div>


                    <!-- Search bar -->
                    <div class="mx-3">
                        <input id="search" name="search" class="form-control search-input rounded-pill" type="text"
                            placeholder="Search" autocomplete="off">
                    </div>

                    <!-- Export -->
                    <div>

                        <button data-bs-toggle="modal" data-bs-target="#exportModal"
                            class="btn px-3 btn-secondary rounded-pill">
                            <span class="d-flex">Export&nbsp;<i class="bi bi-box-arrow-up-right"></i></span>
                        </button>

                        <!-- Export Modal -->
                        <div class="modal fade" id="exportModal" tabindex="-1" aria-labelledby="exportModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Export</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row align-items-center justify-content-center">
                                            <div class="col-6">
                                                Export only book titles
                                            </div>
                                            <div class="col-4">
                                                <div class="btn-group" role="group"
                                                    aria-label="Basic mixed styles example">
                                                    <a type="button" class="btn btn-success"
                                                        href="{{ route('books.export.title.csv') }}">.csv</a>
                                                    <a type="button" href="{{ route('books.export.title.xml') }}"
                                                        class="btn btn-warning">.xml</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row align-items-center justify-content-center mt-3">
                                            <div class="col-6">
                                                Export only book authors
                                            </div>
                                            <div class="col-4">
                                                <div class="btn-group" role="group"
                                                    aria-label="Basic mixed styles example">
                                                    <a type="button" class="btn btn-success"
                                                        href="{{ route('books.export.author.csv') }}">.csv</a>
                                                    <a type="button" href="{{ route('books.export.author.xml') }}"
                                                        class="btn btn-warning">.xml</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row align-items-center justify-content-center mt-3">
                                            <div class="col-6">
                                                Export book titles & authors
                                            </div>
                                            <div class="col-4">
                                                <div class="btn-group" role="group"
                                                    aria-label="Basic mixed styles example">
                                                    <a type="button" class="btn btn-success"
                                                        href="{{ route('books.export.csv') }}">.csv</a>
                                                    <a type="button" href="{{ route('books.export.xml') }}"
                                                        class="btn btn-warning">.xml</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!--table-->
            <div class="mt-3 table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col" style="width: 7%"><span class="d-flex">@sortablelink('ID')
                                    &nbsp;<i class="bi bi-sort-down"></i></span>
                            </th>
                            <th scope="col" style="width: 23%">
                                <span class="d-flex">@sortablelink('title')
                                    &nbsp;<i class="bi bi-sort-down"></i></span>
                            </th>
                            <th scope="col" style="width: 20%">
                                <span class="d-flex">@sortablelink('author')
                                    &nbsp;<i class="bi bi-sort-down"></i></span>
                            </th>
                            <th scope="col" style="width: 40%" class="d-none d-lg-table-cell">Description</th>
                            <th scope="col" style="width: 10%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($books->count() == 0)
                        <tr>
                            <td colspan="5" class="text-center">No books to display</td>
                        </tr>
                        @endif

                        @foreach ($books as $book)
                        <tr>
                            <th scope="row">{{$book->id}}</th>
                            <td>{{$book->title}}</td>
                            <td>{{$book->author}}</td>
                            <td class="d-none d-lg-table-cell">{{$book->description}}</td>
                            <td>@include('books.action')</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="d-flex justify-content-end">
                    {!! $books->appends(Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('search-script')
<script type="text/javascript">
$('#search').on('keyup', function() {
    $value = $(this).val();
    $.ajax({
        type: 'GET',
        url: "{{URL::to('books/search')}}",
        data: {
            'search': $value
        },
        success: function(data) {
            $('tbody').html(data);
        }
    });
})
</script>
<script type="text/javascript">
$.ajaxSetup({
    headers: {
        'csrftoken': '{{ csrf_token() }}'
    }
});
</script>
@endsection