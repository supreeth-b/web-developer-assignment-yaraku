@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card rounded shadow">

                <div class="card-header px-4 py-3">
                    <div class="d-flex justify-content-between">
                        <div class="title">
                            <h4>Add Book</h4>
                        </div>
                        <div>
                            <a href="{{route('books.index')}}" class="btn btn-success rounded-pill">Back</a>
                        </div>
                    </div>
                </div>

                <div class="card-body px-4">
                    <form action="{{route('books.store')}}" method="post">
                        @csrf
                        @method('POST')
                        <div class="mb-4">
                            <label for="title" class="form-label">Title</label>
                            <input type="text" class="form-control" name="title" value="{{old('title')}}"
                                placeholder="Enter book title">
                            @if($errors->any('title'))
                            <span class="text-danger"> {{$errors->first('title')}}</span>
                            @endif
                        </div>

                        <div class="mb-4">
                            <label for="author" class="form-label">Author</label>
                            <input type="text" class="form-control" name="author" value="{{old('author')}}"
                                placeholder="Enter book author">
                            @if($errors->any('author'))
                            <span class="text-danger"> {{$errors->first('author')}}</span>
                            @endif
                        </div>

                        <div class="mb-4">
                            <label for="description" class="form-label">Description</label>
                            <textarea type="text" class="form-control" name="description" value="{{old('description')}}"
                                placeholder="Enter description"></textarea>
                            @if($errors->any('description'))
                            <span class="text-danger"> {{$errors->first('description')}}</span>
                            @endif
                        </div>

                        <div class="mb-3">
                            <button type="submit" class="btn btn-primary rounded-pill">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection