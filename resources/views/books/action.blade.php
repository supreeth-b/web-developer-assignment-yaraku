<div class="d-flex">

    <!-- Edit Author -->
    <div>
        <button title="edit" data-bs-toggle="modal" data-bs-target="#authorEditModal{{ $book->id }}" class="btn">
            <i class="bi bi-pencil-square text-secondary"></i>
        </button>

        <!-- The author edit modal -->
        <div id="authorEditModal{{ $book->id }}" class="modal fade" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Author
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <div class="modal-body">
                        <form action="{{ route('books.update',$book->id) }}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="mb-4">
                                <label for="author" class="form-label">Author</label>
                                <input type="text" class="form-control" name="author" value="{{old('author')}}"
                                    placeholder="Enter author name">
                                @if($errors->any('author'))
                                <span class="text-danger">
                                    {{$errors->first('author')}}</span>
                                @endif
                            </div>

                            <div>
                                <button type="submit" class="btn btn-primary rounded-pill">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Book -->
    <div>
        <form action="{{ route('books.destroy',$book->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <button class="btn" title="delete" type="submit"><i class="bi bi-trash-fill text-danger"></i></button>
        </form>
    </div>
</div>