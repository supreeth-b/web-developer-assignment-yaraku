<nav class="navbar navbar-expand-lg navbar-light mb-2">
    <div class="container">
        <a class="navbar-brand  me-auto ms-auto text-center" href="/">
            <img itemprop="image" class="img-fluid w-50"
                src="https://www.yaraku.com/wp-content/uploads/sites/3/2018/06/yaraku-logo_200px-compressor.png"
                alt="Logo">
        </a>
    </div>
</nav>